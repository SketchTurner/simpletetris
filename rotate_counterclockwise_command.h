#ifndef ROTATECOUNTERCLOCKWISECOMMAND_H
#define ROTATECOUNTERCLOCKWISECOMMAND_H

#include "command.h"

class RotateCounterclockwiseCommand : public Command
{
public:
    RotateCounterclockwiseCommand(Figure* &fig);
    ~RotateCounterclockwiseCommand();
    virtual void execute() override;
};

#endif // ROTATECOUNTERCLOCKWISECOMMAND_H
