#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QObject>
#include <QTimer>
#include <QtMath>

#include <map>
#include <queue>
#include <ctime>

#include "cell.h"
#include "command_headers.h"

class GameManager: public QObject
{
    Q_OBJECT

private:
    static const int matrixWidth = 10;
    static const int matrixHeight = 20;
    Cell matrix[matrixHeight][matrixWidth];
    Figure* currentFigure;
    Figure* nextFigure;
    QTimer* timer;
    Command *cmdMoveLeft,
            *cmdMoveRight,
            *cmdMoveDown,
            *cmdMoveUp,
            *cmdRotateClockwise,
            *cmdRotateCounterClockwise;
    bool paused = true;
    bool alive = false;
    int currentLevel;
    int currentScore;
    int currentLines;

signals:
    void figureMoved();
    void userLostGame();

public slots:
    void timer_overflow();

public:
    GameManager();
    ~GameManager();

    void newGame();
    void pauseUnpause();
    bool moveDown();
    void moveLeft();
    void moveRight();
    void rotateClockwise();
    void rotateCounterClockwise();
    void dropFigure();

    bool isGamePaused() { return paused; }
    bool isGameAlive() { return alive; }

    Cell getCell(int y, int x);
    multimap<int, int> getFilledPositions(FigureNumber fig, CellColor* color);
    int getCurrentLevel() { return currentLevel; }
    int getCurrentLines() { return currentLines; }
    int getCurrentScore() { return currentScore; }

private:
    Figure* createRandomFigure();
    bool intersects();
    bool queueContains(queue<int> &q, int n);
    void lockFigureIn();
    int calculateDelay();
    void calculateLevel();
    void findLines();
    bool executeCommand(Command* &cmd);
    void moveUp();
};

#endif // GAMEMANAGER_H
