#include "figure.h"

#include "QDebug"

int Figure::_width = 0;

Figure::Figure(FigureType type)
{
    switch (type) {
    case Q:
        stateCount = 1;
        states = new int[stateCount] {0x6600};
        color = CellColor::Yellow;
        break;
    case S:
        stateCount = 2;
        states = new int[stateCount] {0x6C00, 0x4620};
        color = CellColor::Green;
        break;
    case Z:
        stateCount = 2;
        states = new int[stateCount] {0xC600, 0x2640};
        color = CellColor::DeepPurple;
        break;
    case T:
        stateCount = 4;
        states = new int[stateCount] {0x4E00, 0x4640, 0x0E40, 0x4C40};
        color = CellColor::Pink;
        break;
    case L:
        stateCount = 4;
        states = new int[stateCount] {0x2E00, 0x4460, 0x0E80, 0xC440};
        color = CellColor::DeepOrange;
        break;
    case J:
        stateCount = 4;
        states = new int[stateCount] {0x8E00, 0x6440, 0x0E20, 0x44C0};
        color = CellColor::Teal;
        break;
    case I:
        stateCount = 4;
        states = new int[stateCount] {0x0F00, 0x2222, 0x00F0, 0x4444};
        color = CellColor::Blue;
        break;
    default:
        break;
    }

    setState(rand() % 4);

    int y = 1 - matrixSize;
    int x = _width / 2;
    int tmpState = states[currentStateNumber];
    if (tmpState & 0x2222)
        x--;
    for (int i = 0; i < matrixSize; i++)
    {
        if (tmpState % 16)
            break;
        y++;
        tmpState = tmpState >> 4;
    }
    posX = x; posY = y;
}

Figure::~Figure()
{
    delete states;
}

void Figure::init(int width)
{
    _width = width;
}

void Figure::move(MoveDirection direction)
{
    switch (direction) {
    case Left:
        posX--;
        break;
    case Right:
        posX++;
        break;
    case Down:
        posY++;
        break;
    case Up:
        posY--;
        break;
    default:
        break;
    }
}

void Figure::rotate(RotateDirection direction)
{
    switch (direction) {
    case CounterClockwise:
        setState(currentStateNumber - 1);
        break;
    case Clockwise:
        setState(currentStateNumber + 1);
        break;
    default:
        break;
    }
}

multimap<int, int> Figure::getFilledPositions(FigureNumber fig)
{
    multimap<int, int> positionsMap;
    int state = states[currentStateNumber];
    for (int i = matrixSize - 1; i >= 0; i--)
    {
        for (int j = matrixSize - 1; j >=0; j--)
        {
            if (state % 2)
            {
                if (fig == FigureNumber::CurrentFigure)
                    positionsMap.insert(pair<int, int>(posY + i, posX + j));
                else    // FigureNumber::NextFigure
                    positionsMap.insert(pair<int, int>(i, j));
            }
            state = state >> 1;
        }
    }
    return positionsMap;
}

bool Figure::isAboveTopEdge()
{
    return getTopY() < 0;
}

int Figure::getTopY()
{
    int state = states[currentStateNumber];
    for (int i = 0, mask = 0xF000; i < matrixSize; i++, mask = mask >> 4)
    {
        if (state & mask)
            return posY + i;
    }
    return 0;
}

int Figure::getBottomY()
{
    int state = states[currentStateNumber];
    for (int i = matrixSize - 1; i >= 0; i--)
    {
        if (state % 16)
            return posY + i;
        state = state >> 4;
    }
    return 0;
}

void Figure::setState(int state)
{
    currentStateNumber = (state + stateCount) % stateCount;
}

