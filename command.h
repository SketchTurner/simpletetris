#ifndef COMMAND_H
#define COMMAND_H

#include "figure.h"

#include <QDebug>

class Command
{

private:
    int figPosX, figPosY;
    int figCurState;

protected:
    Figure** figure;

public:
    Command(Figure* &fig);
    virtual ~Command();
    virtual void execute();
    void unExecute();

private:
    void saveState();
    void restoreState();
};

#endif // COMMAND_H
