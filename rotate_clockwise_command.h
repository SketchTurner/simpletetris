#ifndef ROTATECLOCKWISECOMMAND_H
#define ROTATECLOCKWISECOMMAND_H

#include "command.h"

class RotateClockwiseCommand : public Command
{
public:
    RotateClockwiseCommand(Figure* &fig);
    ~RotateClockwiseCommand();
    virtual void execute() override;
};

#endif // ROTATECLOCKWISECOMMAND_H
