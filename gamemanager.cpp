#include "gamemanager.h"

void GameManager::timer_overflow()
{
    moveDown();
}

GameManager::GameManager()
{
    currentFigure = nullptr;
    nextFigure = nullptr;
    timer = new QTimer;
    Figure::init(matrixWidth);
    srand(time(nullptr));

    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(timer_overflow()));

    cmdMoveLeft = new MoveLeftCommand(currentFigure);
    cmdMoveRight = new MoveRightCommand(currentFigure);
    cmdMoveDown = new MoveDownCommand(currentFigure);
    cmdMoveUp = new MoveUpCommand(currentFigure);
    cmdRotateClockwise = new RotateClockwiseCommand(currentFigure);
    cmdRotateCounterClockwise = new RotateCounterclockwiseCommand(currentFigure);
}

GameManager::~GameManager()
{
    if (currentFigure)
        delete currentFigure;
    if (nextFigure)
        delete nextFigure;

    QObject::disconnect(timer, SIGNAL(timeout()), this, SLOT(timer_overflow()));

    delete timer;
    delete cmdMoveLeft;
    delete cmdMoveRight;
    delete cmdMoveDown;
    delete cmdRotateClockwise;
    delete cmdRotateCounterClockwise;
}

Cell GameManager::getCell(int y, int x)
{
    return matrix[y][x];
}

multimap<int, int> GameManager::getFilledPositions(FigureNumber fig, CellColor *color)
{
    if (fig == FigureNumber::CurrentFigure)
    {
        if (nullptr != currentFigure)
        {
            *color = currentFigure->getColor();
            return currentFigure->getFilledPositions(fig);
        }
    }
    else
    {
        if (nullptr != currentFigure)
        {
            *color = nextFigure->getColor();
            return nextFigure->getFilledPositions(fig);
        }
    }
    multimap<int, int> m;
    return m;
}

void GameManager::newGame()
{
    if (currentFigure)
        delete currentFigure;
    if (nextFigure)
        currentFigure = nextFigure;
    else
        currentFigure = createRandomFigure();
    nextFigure = createRandomFigure();

    for (int i = 0; i < matrixHeight; i++)
    {
        for (int j = 0; j < matrixWidth; j++)
        {
            matrix[i][j].color = CellColor::Transparent;
            matrix[i][j].isFilled = false;
        }
    }

    currentLevel = 1;
    currentLines = 0;
    currentScore = 0;
    paused = true;
    alive = true;
    pauseUnpause();
}

void GameManager::pauseUnpause()
{
    if (alive)
    {
        if (paused)
        {
            timer->start(calculateDelay());
        }
        else
        {
            timer->stop();
        }
        paused = !paused;
    }
    emit figureMoved();
}

Figure *GameManager::createRandomFigure()
{
    return new Figure(
                static_cast<FigureType>(
                    rand() % (FigureType::MAX_TYPE_NUMBER + 1)
                    )
                );
}

bool GameManager::intersects()
{
    auto positions = currentFigure->getFilledPositions(FigureNumber::CurrentFigure);
    for (const auto pos: positions)
    {
        if (pos.first >= matrixHeight ||
            pos.second < 0 || pos.second >= matrixWidth)
                return true;
        if (pos.first < 0)
            continue;
        if (matrix[pos.first][pos.second].isFilled)
            return true;
    }
    return false;
}

bool GameManager::queueContains(queue<int> &q, int n)
{
    bool contains = false;
    for (int i = 0; i < static_cast<FigureType>(q.size()); i++)
    {
        int tmp = q.front();
        if (n == tmp)
            contains = true;
        q.pop();
        q.push(tmp);
    }
    return contains;
}

void GameManager::lockFigureIn()
{
    CellColor color = currentFigure->getColor();
    auto positions = currentFigure->getFilledPositions(FigureNumber::CurrentFigure);
    for (const auto pos: positions)
    {
        matrix[pos.first][pos.second].isFilled = true;
        matrix[pos.first][pos.second].color = color;
    }
}

int GameManager::calculateDelay()
{
    return 1000 * qPow(0.9, currentLevel - 1);
}

void GameManager::calculateLevel()
{
    currentLevel = currentLines / 5 + 1;
    timer->setInterval(calculateDelay());
}

void GameManager::findLines()
{
    int linesCount = 0;
    int topY = currentFigure->getTopY();
    int bottomY = currentFigure->getBottomY();
    queue<int> obsoleteLines;
    for (int i = bottomY; i >= topY; i--)
    {
        for (int j = 0; j < matrixWidth; j++)
        {
            if (!matrix[i][j].isFilled)
                break;
            if (j == matrixWidth - 1)
            {
                linesCount++;
                obsoleteLines.push(i);
            }
        }
    }

    while(!obsoleteLines.empty())
    {
        int insertionLine = obsoleteLines.front();
        obsoleteLines.pop();
        bool inserted = false;
        for (int i = insertionLine - 1; i >= 0; i--)
        {
            if (queueContains(obsoleteLines, i))
                continue;
            for (int j = 0; j < matrixWidth; j++)
            {
                matrix[insertionLine][j] = matrix[i][j];
            }
            obsoleteLines.push(i);
            inserted = true;
            break;
        }
        if (!inserted)
        {
            for (int j = 0; j < matrixHeight; j++)
            {
                matrix[insertionLine][j].isFilled = false;
                matrix[insertionLine][j].color = CellColor::Transparent;
            }
        }
    }
    if (linesCount)
    {
        currentLines += linesCount;
        currentScore += linesCount * 10;
        calculateLevel();
    }
}

bool GameManager::executeCommand(Command* &cmd)
{
    if (alive && !paused)
    {
        cmd->execute();
        if (intersects())
        {
            cmd->unExecute();
            return false;
        }
        emit figureMoved();
    }
    return true;
}

bool GameManager::moveDown()
{
    if (!executeCommand(cmdMoveDown))
    {
        if (currentFigure->isAboveTopEdge()) {
            pauseUnpause();
            alive = false;
            emit figureMoved();
            emit userLostGame();
        }
        else
        {
            lockFigureIn();
            findLines();
            delete currentFigure;
            currentFigure = nextFigure;
            if (intersects())
                moveUp();
            nextFigure = createRandomFigure();
            emit figureMoved();
        }
        return false;
    }
    return true;
}

void GameManager::moveLeft()
{
    executeCommand(cmdMoveLeft);
}

void GameManager::moveUp()
{
    executeCommand(cmdMoveUp);
}

void GameManager::moveRight()
{
    executeCommand(cmdMoveRight);
}

void GameManager::rotateClockwise()
{
    executeCommand(cmdRotateClockwise);
}

void GameManager::rotateCounterClockwise()
{
    executeCommand(cmdRotateCounterClockwise);
}

void GameManager::dropFigure()
{
    while (alive && !paused && moveDown());
    timer->setInterval(calculateDelay());
}

