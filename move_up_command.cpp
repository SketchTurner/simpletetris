#include "move_up_command.h"

MoveUpCommand::MoveUpCommand(Figure* &fig) : Command(fig)
{

}

MoveUpCommand::~MoveUpCommand()
{

}

void MoveUpCommand::execute()
{
    Command::execute();
    (*figure)->move(MoveDirection::Up);
}
