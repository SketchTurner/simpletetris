#include "move_down_command.h"

MoveDownCommand::MoveDownCommand(Figure* &fig) : Command(fig)
{

}

MoveDownCommand::~MoveDownCommand()
{

}

void MoveDownCommand::execute()
{
    Command::execute();
    (*figure)->move(MoveDirection::Down);
}
