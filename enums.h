#ifndef ENUMS
#define ENUMS

enum FigureType
{
    Q, //square
    S,
    Z,
    T,
    L,
    J,
    I,  //stick
    MAX_TYPE_NUMBER = I
};

enum FigureNumber
{
    CurrentFigure,
    NextFigure
};

enum CellColor
{
    Green,
    Yellow,
    Pink,
    Blue,
    DeepOrange,
    DeepPurple,
    Teal,
    Transparent
};

enum RotateDirection
{
    Clockwise,
    CounterClockwise
};

enum MoveDirection
{
    Left,
    Right,
    Down,
    Up
};

#endif // ENUMS

