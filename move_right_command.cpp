#include "move_right_command.h"

MoveRightCommand::MoveRightCommand(Figure* &fig) : Command(fig)
{

}

MoveRightCommand::~MoveRightCommand()
{

}

void MoveRightCommand::execute()
{
    Command::execute();
    (*figure)->move(MoveDirection::Right);
}
