#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QDebug"
#include <QMouseEvent>
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color: black;");
    mgr = new GameManager();
    scene = new QGraphicsScene();
    initGraphicsTools();
    ui->gvScreen->setScene(scene);
    ui->gvScreen->viewport()->installEventFilter(this);
    QObject::connect(mgr, SIGNAL(figureMoved()), this, SLOT(onFigureMoved()));
    QObject::connect(mgr, SIGNAL(userLostGame()), this, SLOT(onUserLostGame()));
}

MainWindow::~MainWindow()
{
    QObject::disconnect(mgr, SIGNAL(figureMoved()), this, SLOT(onFigureMoved()));
    QObject::disconnect(mgr, SIGNAL(userLostGame()), this, SLOT(onUserLostGame()));
    delete gNewGameText;
    delete gLevelText;
    delete gScoreText;
    delete scene;
    delete ui;
    delete mgr;
}

void MainWindow::onFigureMoved()
{
    drawScreen();
}

void MainWindow::onUserLostGame()
{
    drawDefeatMessage();
}

void MainWindow::recomputeDimensions()
{
    if (ui->gvScreen->viewport()->width() == lastWidth &&
        ui->gvScreen->viewport()->height() == lastHeight)
            return;

    lastWidth = ui->gvScreen->viewport()->width();
    lastHeight = ui->gvScreen->viewport()->height();

    if (lastWidth >= lastHeight)
        cellSize = (lastHeight - heightInCells - 1) / heightInCells;
    else
        cellSize = ((lastWidth / 2) - widthInCells - 1) / widthInCells;

    int fieldWidth = (cellSize + 1) * widthInCells;
    int fieldHeight = (cellSize + 1) * heightInCells;

    rectField.setLeft((lastWidth - fieldWidth) / 2);
    rectField.setTop((lastHeight - fieldHeight) / 2);
    rectField.setWidth(fieldWidth);
    rectField.setHeight(fieldHeight);

    int nextFieldSize = (cellSize + 1) * nextFieldSizeInCells + 1;

    rectNext.setLeft((lastWidth + rectField.right() - nextFieldSize) / 2);
    rectNext.setTop(rectField.top());
    rectNext.setWidth(nextFieldSize);
    rectNext.setHeight(nextFieldSize);

    rectBtnNewGame.setLeft((rectField.left() - nextFieldSize) / 2);
    rectBtnNewGame.setTop(rectField.top());
    rectBtnNewGame.setWidth(nextFieldSize);
    rectBtnNewGame.setHeight(2 * cellSize);

    rectBtnPause.setLeft(rectBtnNewGame.left());
    rectBtnPause.setTop(rectField.top() + 2.5 * cellSize);
    rectBtnPause.setWidth(nextFieldSize);
    rectBtnPause.setHeight(rectBtnNewGame.height());

    rectBtnRotateCCW.setLeft((rectField.left() - 3 * cellSize) / 2);
    rectBtnRotateCCW.setTop(rectField.bottom() - 8 * (cellSize + 1));
    rectBtnRotateCCW.setWidth(3 * (cellSize + 1));
    rectBtnRotateCCW.setHeight(3 * (cellSize + 1));

    rectBtnMoveLeft.setLeft(rectBtnRotateCCW.left());
    rectBtnMoveLeft.setTop(rectBtnRotateCCW.top() + 3.5 * cellSize);
    rectBtnMoveLeft.setWidth(rectBtnRotateCCW.width());
    rectBtnMoveLeft.setHeight(rectBtnRotateCCW.height());

    rectBtnRotateCW.setLeft((lastWidth + rectField.right() - 3 * cellSize) / 2 );
    rectBtnRotateCW.setTop(rectBtnRotateCCW.top());
    rectBtnRotateCW.setWidth(rectBtnRotateCCW.width());
    rectBtnRotateCW.setHeight(rectBtnRotateCCW.height());

    rectBtnMoveRight.setLeft(rectBtnRotateCW.left());
    rectBtnMoveRight.setTop(rectBtnMoveLeft.top());
    rectBtnMoveRight.setWidth(rectBtnRotateCCW.width());
    rectBtnMoveRight.setHeight(rectBtnRotateCCW.height());

    int tmpWidth = rectBtnPause.height() / 2;
    rectPauseIcon.setLeft(rectBtnPause.left() + (rectBtnPause.width() - tmpWidth) / 2);
    rectPauseIcon.setTop(rectBtnPause.top() + (rectBtnPause.height() - tmpWidth) / 2);
    rectPauseIcon.setWidth(tmpWidth);
    rectPauseIcon.setHeight(tmpWidth);

    polygonPlayIcon.clear();
    polygonPlayIcon.append(QPointF(rectPauseIcon.left(), rectPauseIcon.top()));
    polygonPlayIcon.append(QPointF(rectPauseIcon.left(), rectPauseIcon.bottom()));
    polygonPlayIcon.append(QPointF(rectPauseIcon.right(),
                                       (rectPauseIcon.top() + rectPauseIcon.bottom()) / 2));

    gInfoFont.setPixelSize(cellSize / 2);
}

void MainWindow::drawScreen()
{
    scene->clear();
    scene->setSceneRect(0, 0, lastWidth - 1, lastHeight - 1);
    scene->setBackgroundBrush(gBlackBrush);
    scene->addRect(rectField, gGrayPen, gGrayBrush);
    scene->addRect(rectNext, gGrayPen, gGrayBrush);
    scene->addRect(rectBtnNewGame, gGrayPen, gGrayBrush);

    if (touchInterfaceOn)
    {
        scene->addRect(rectBtnRotateCCW, gGrayPen, gGrayBrush);
        scene->addRect(rectBtnMoveLeft, gGrayPen, gGrayBrush);
        scene->addRect(rectBtnRotateCW, gGrayPen, gGrayBrush);
        scene->addRect(rectBtnMoveRight, gGrayPen, gGrayBrush);
    }

    drawGrid(FigureNumber::CurrentFigure);
    drawGrid(FigureNumber::NextFigure);

    if (mgr->isGameAlive())
    {
        scene->addRect(rectBtnPause, gGrayPen, gGrayBrush);
        if (mgr->isGamePaused())
            scene->addPolygon(polygonPlayIcon, gTransparentPen, gTealBrush);
        else
        {
            int tmpWidth = rectPauseIcon.width() / 3;
            scene->addRect(rectPauseIcon.left(),
                           rectPauseIcon.top(),
                           tmpWidth,
                           rectPauseIcon.height(),
                           gTransparentPen,
                           gYellowBrush);
            scene->addRect(rectPauseIcon.right() - tmpWidth,
                           rectPauseIcon.top(),
                           tmpWidth,
                           rectPauseIcon.height(),
                           gTransparentPen,
                           gYellowBrush);
        }

        setScoreText();
        setLevelText();
        setLinesText();
        scene->addItem(gScoreText);
        scene->addItem(gLevelText);
        scene->addItem(gLinesText);
    }

    setNewGameText();
    scene->addItem(gNewGameText);

    QPixmap mapRotLeft = QPixmap(":sprites/images/rotate-left.png");
    mapRotLeft = mapRotLeft.scaledToWidth(rectBtnRotateCCW.width());
    scene->addPixmap(mapRotLeft)->moveBy(rectBtnRotateCCW.left(), rectBtnRotateCCW.top());

    QPixmap mapRotRight = QPixmap(":sprites/images/rotate-right.png");
    mapRotRight = mapRotRight.scaledToWidth(rectBtnRotateCW.width());
    scene->addPixmap(mapRotRight)->moveBy(rectBtnRotateCW.left(), rectBtnRotateCW.top());

    QPixmap mapMoveLeft = QPixmap(":sprites/images/move-left.png");
    mapMoveLeft = mapMoveLeft.scaledToWidth(rectBtnMoveLeft.width());
    scene->addPixmap(mapMoveLeft)->moveBy(rectBtnMoveLeft.left(), rectBtnMoveLeft.top());

    QPixmap mapMoveRight = QPixmap(":sprites/images/move-right.png");
    mapMoveRight = mapMoveRight.scaledToWidth(rectBtnMoveRight.width());
    scene->addPixmap(mapMoveRight)->moveBy(rectBtnMoveRight.left(), rectBtnMoveRight.top());

    drawFigure(FigureNumber::CurrentFigure);
    drawCells();
    drawFigure(FigureNumber::NextFigure);
}

void MainWindow::askForNewGame()
{
    if (mgr->isGameAlive())
    {
        //TODO: ask for current game interruption and new game creating
    }
    else
    {
        mgr->newGame();
    }
}

void MainWindow::pauseGame()
{
    if (mgr->isGameAlive())
    {
        mgr->pauseUnpause();
    }
}

void MainWindow::dropFigure()
{
    mgr->dropFigure();
}

void MainWindow::moveFigureLeft()
{
    mgr->moveLeft();
}

void MainWindow::moveFigureRight()
{
    mgr->moveRight();
}

void MainWindow::rotateFigureCW()
{
    mgr->rotateClockwise();
}

void MainWindow::rotateFigureCCW()
{
    mgr->rotateCounterClockwise();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        QPoint remapped = ui->gvScreen->mapFromParent(event->pos());
        if ( ui->gvScreen->rect().contains(remapped))
        {
            QPointF mousePoint = ui->gvScreen->mapToScene(remapped);
            QPoint pt = mousePoint.toPoint();
            if (rectBtnNewGame.contains(pt))
            {
                askForNewGame();
            }
            else if (rectBtnPause.contains(pt))
            {
                pauseGame();
            }
            else if (rectField.contains(pt))
            {
                dropFigure();
            }
            else if (touchInterfaceOn)
            {
                if (rectBtnRotateCCW.contains(pt))
                {
                    rotateFigureCCW();
                }
                if (rectBtnMoveLeft.contains(pt))
                {
                    moveFigureLeft();
                }
                if (rectBtnRotateCW.contains(pt))
                {
                    rotateFigureCW();
                }
                if (rectBtnMoveRight.contains(pt))
                {
                    moveFigureRight();
                }
            }
        }
    }
    this->setFocus();
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == ui->gvScreen->viewport() && event->type() == QEvent::Resize)
    {
        recomputeDimensions();
        drawScreen();
    }
    return QMainWindow::eventFilter(watched, event);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Up:
        rotateFigureCW();
        break;
    case Qt::Key_Left:
        moveFigureLeft();
        break;
    case Qt::Key_Right:
        moveFigureRight();
        break;
    case Qt::Key_Down:
        dropFigure();
        break;
    case Qt::Key_Space:
        pauseGame();
    default:
        break;
    }
}

void MainWindow::initGraphicsTools()
{
    gGrayPen.setColor(Qt::gray);
    gGrayPen.setStyle(Qt::SolidLine);
    gTransparentPen.setColor(Qt::transparent);
    gTransparentPen.setStyle(Qt::SolidLine);

    gGreenBrush.setColor(QColor(0x4B, 0xAE, 0x4F));
    gGreenBrush.setStyle(Qt::SolidPattern);
    gYellowBrush.setColor(QColor(0xFF, 0xEB, 0x3B));
    gYellowBrush.setStyle(Qt::SolidPattern);
    gPinkBrush.setColor(QColor(0xFF, 0x40, 0x81));
    gPinkBrush.setStyle(Qt::SolidPattern);
    gBlueBrush.setColor(QColor(0x44, 0x8A, 0xFF));
    gBlueBrush.setStyle(Qt::SolidPattern);
    gDeepOrangeBrush.setColor(QColor(0xFF, 0x57, 0x22));
    gDeepOrangeBrush.setStyle(Qt::SolidPattern);
    gDeepPurpleBrush.setColor(QColor(0x66, 0x39, 0xB6));
    gDeepPurpleBrush.setStyle(Qt::SolidPattern);
    gTealBrush.setColor(QColor(0x00, 0x95, 0x87));
    gTealBrush.setStyle(Qt::SolidPattern);
    gBlackBrush.setColor(Qt::black);
    gBlackBrush.setStyle(Qt::SolidPattern);
    gGrayBrush.setColor(QColor(0x36, 0x3F, 0x45));
    gGrayBrush.setStyle(Qt::SolidPattern);

    gNewGameText = new QGraphicsTextItem("New Game");
    gNewGameText->setDefaultTextColor(QColor(0x00, 0xBB, 0xD3));
    gScoreText = new QGraphicsTextItem();
    gLevelText = new QGraphicsTextItem();

    gInfoFont.setFamily("Helvetica");
    gInfoFont.setBold(true);
}

void MainWindow::drawGrid(FigureNumber fig)
{
    QPoint startPos;
    int vertLinesCount, horLinesCount;
    if (fig == FigureNumber::CurrentFigure)
    {
        startPos = rectField.topLeft();
        vertLinesCount = widthInCells - 1;
        horLinesCount = heightInCells - 1;
    }
    else    // FigureNumber::NextFigure
    {
        startPos = rectNext.topLeft();
        vertLinesCount = horLinesCount = nextFieldSizeInCells - 1;
    }
    int horLineLength = (cellSize + 1) * (vertLinesCount + 1);
    int vertLineLength = (cellSize + 1) * (horLinesCount + 1);
    for (int i = 1; i <= horLinesCount; i++)
    {
        int posY = startPos.y() + i * (cellSize + 1);
        scene->addLine(startPos.x(), posY, startPos.x() + horLineLength, posY,  gGrayPen);
    }
    for (int j = 1; j <= vertLinesCount; j++)
    {
        int posX = startPos.x() + j * (cellSize + 1);
        scene->addLine(posX, startPos.y(), posX, startPos.y() + vertLineLength, gGrayPen);
    }
}

void MainWindow::drawFigure(FigureNumber fig)
{
    QPoint startPos;
    if (fig == FigureNumber::CurrentFigure)
    {
        startPos = rectField.topLeft();
    }
    else    // FigureNumber::NextFigure
    {
        startPos = rectNext.topLeft();
    }
    CellColor color;
    multimap<int, int> positions = mgr->getFilledPositions(fig, &color);
    QBrush* brush = getBrushByColor(color);
    for (const auto pos: positions)
    {
        if (pos.first < 0)
            continue;
        scene->addRect(startPos.x() + 1 + pos.second * (cellSize + 1),
                       startPos.y() + 1 + pos.first * (cellSize + 1),
                       cellSize, cellSize,
                       gTransparentPen,
                       *brush);
    }
}

void MainWindow::drawCells()
{
    int startPosX = rectField.left() + 1;
    int startPosY = rectField.top() + 1;

    for (int i = 0; i < heightInCells; i++)
    {
        for (int j = 0; j < widthInCells; j++)
        {
            Cell c = mgr->getCell(i, j);
            if (c.isFilled)
            {
                QBrush* brush = getBrushByColor(c.color);
                scene->addRect(startPosX + j * (cellSize + 1),
                               startPosY + i * (cellSize + 1),
                               cellSize, cellSize,
                               gTransparentPen,
                               *brush);
            }
        }
    }
}

void MainWindow::drawDefeatMessage()
{
    //TODO: draw defeat message
}

void MainWindow::setNewGameText()
{
    gNewGameText = new QGraphicsTextItem("New game");
    gNewGameText->setFont(gInfoFont);
    QRectF rect = gNewGameText->boundingRect();
    setupTextItem(gNewGameText,
                  rectBtnNewGame.left() + (rectBtnNewGame.width() - rect.width()) / 2,
                  rectBtnNewGame.top() + (rectBtnNewGame.height() - rect.height()) / 2,
                  QColor(0x20, 0xD0, 0x20)
    );
}

void MainWindow::setScoreText()
{
    gScoreText = new QGraphicsTextItem(QString("Score:\t%1").arg(mgr->getCurrentScore()));
    setupTextItem(gScoreText,
                  rectNext.left(),
                  rectNext.bottom() + cellSize
    );
}

void MainWindow::setLevelText()
{
    gLevelText = new QGraphicsTextItem(QString("Level:\t%1").arg(mgr->getCurrentLevel()));
    setupTextItem(gLevelText,
                  rectNext.left(),
                  rectNext.bottom() + 2 * cellSize
    );
}

void MainWindow::setLinesText()
{
    gLinesText = new QGraphicsTextItem(QString("Lines:\t%1").arg(mgr->getCurrentLines()));
    setupTextItem(gLinesText,
                  rectNext.left(),
                  rectNext.bottom() + 3 * cellSize
    );
}

void MainWindow::setupTextItem(QGraphicsTextItem *item, int left, int top, QColor color)
{
    item->setFont(gInfoFont);
    item->setDefaultTextColor(color);
    item->setPos(left, top);
}

QBrush* MainWindow::getBrushByColor(CellColor color)
{
    switch (color)
    {
        case CellColor::Green :
            return &gGreenBrush;
            break;
        case CellColor::Yellow :
            return &gYellowBrush;
            break;
        case CellColor::Pink :
            return &gPinkBrush;
            break;
        case CellColor::Blue :
            return &gBlueBrush;
            break;
        case CellColor::DeepOrange :
            return &gDeepOrangeBrush;
            break;
        case CellColor::DeepPurple :
            return &gDeepPurpleBrush;
            break;
        case CellColor::Teal :
            return &gTealBrush;
            break;
        default:
            return new QBrush(Qt::transparent);
            break;
    }
}
