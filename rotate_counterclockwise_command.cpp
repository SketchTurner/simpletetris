#include "rotate_counterclockwise_command.h"

RotateCounterclockwiseCommand::RotateCounterclockwiseCommand(Figure* &fig) : Command(fig)
{

}

RotateCounterclockwiseCommand::~RotateCounterclockwiseCommand()
{

}

void RotateCounterclockwiseCommand::execute()
{
    Command::execute();
    (*figure)->rotate(RotateDirection::CounterClockwise);
}

