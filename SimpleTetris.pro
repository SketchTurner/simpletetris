#-------------------------------------------------
#
# Project created by QtCreator 2015-04-04T16:20:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleTetris
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    figure.cpp \
    gamemanager.cpp \
    command.cpp \
    move_left_command.cpp \
    move_right_command.cpp \
    move_down_command.cpp \
    rotate_clockwise_command.cpp \
    rotate_counterclockwise_command.cpp \
    move_up_command.cpp

HEADERS  += mainwindow.h \
    figure.h \
    gamemanager.h \
    command.h \
    cell.h \
    enums.h \
    move_left_command.h \
    move_right_command.h \
    move_down_command.h \
    rotate_clockwise_command.h \
    rotate_counterclockwise_command.h \
    command_headers.h \
    move_up_command.h

FORMS    += mainwindow.ui

CONFIG += mobility \
    c++11
MOBILITY = 

RESOURCES += \
    images.qrc

