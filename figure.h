#ifndef FIGURE_H
#define FIGURE_H

#include <map>

#include "enums.h"

using namespace std;

class Figure
{    
friend class Command;

private:
    const int matrixSize = 4;
    static int _width;
    CellColor color;
    int posX;
    int posY;
    int currentStateNumber;
    int* states;
    int stateCount;

public:
    Figure(FigureType type);
    ~Figure();
    static void init(int width);
    void move(MoveDirection direction);
    void rotate(RotateDirection direction);
    multimap<int, int> getFilledPositions(FigureNumber fig);
    bool isAboveTopEdge();
    CellColor getColor() {return color;}
    int getTopY();
    int getBottomY();

private:
    void setState(int stateNumber);
    int getStateCount();
};

#endif // FIGURE_H
