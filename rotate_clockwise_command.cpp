#include "rotate_clockwise_command.h"

RotateClockwiseCommand::RotateClockwiseCommand(Figure* &fig) : Command(fig)
{

}

RotateClockwiseCommand::~RotateClockwiseCommand()
{

}

void RotateClockwiseCommand::execute()
{
    Command::execute();
    (*figure)->rotate(RotateDirection::Clockwise);
}
