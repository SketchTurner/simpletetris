#ifndef CELL_H
#define CELL_H

#include "enums.h"

struct Cell
{
    bool isFilled = false;
    CellColor color = CellColor::Transparent;
};

#endif // CELL_H
