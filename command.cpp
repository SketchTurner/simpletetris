#include "command.h"

Command::Command(Figure* &fig)
{
    figure = &fig;
}

Command::~Command()
{

}

void Command::execute()
{
    saveState();
}

void Command::unExecute()
{
    restoreState();
}

void Command::saveState()
{
    figPosX = (*figure)->posX;
    figPosY = (*figure)->posY;
    figCurState = (*figure)->currentStateNumber;
}

void Command::restoreState()
{
    (*figure)->posX = figPosX;
    (*figure)->posY = figPosY;
    (*figure)->currentStateNumber = figCurState;
}
