#include "move_left_command.h"

MoveLeftCommand::MoveLeftCommand(Figure* &fig): Command(fig)
{

}

MoveLeftCommand::~MoveLeftCommand()
{

}

void MoveLeftCommand::execute()
{
    Command::execute();
    (*figure)->move(MoveDirection::Left);
}

