#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsTextItem>

#include "gamemanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    GameManager* mgr;
    QGraphicsScene* scene;
    static const int widthInCells = 10;
    static const int heightInCells = 20;
    static const int nextFieldSizeInCells = 4;
    static const bool touchInterfaceOn = true;
    int cellSize = 0;
    int lastWidth = 0, lastHeight = 0;

    QRect   rectField,
            rectNext,
            rectBtnNewGame,
            rectBtnPause,
            rectBtnMoveLeft,
            rectBtnMoveRight,
            rectBtnRotateCCW,
            rectBtnRotateCW,
            rectPauseIcon;
    QPolygonF polygonPlayIcon;
    QPen    gGrayPen,
            gTransparentPen;
    QBrush  gGreenBrush,
            gYellowBrush,
            gPinkBrush,
            gBlueBrush,
            gDeepOrangeBrush,
            gDeepPurpleBrush,
            gTealBrush,
            gBlackBrush,
            gGrayBrush;
    QFont   gInfoFont;
    QGraphicsTextItem   *gNewGameText,
                        *gScoreText,
                        *gLevelText,
                        *gLinesText;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onFigureMoved();
    void onUserLostGame();

protected:
    void mousePressEvent(QMouseEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    void initGraphicsTools();
    void recomputeDimensions();
    void drawScreen();
    void drawGrid(FigureNumber fig);
    void drawFigure(FigureNumber fig);
    void drawCells();
    void drawDefeatMessage();
    void setNewGameText();
    void setScoreText();
    void setLevelText();
    void setLinesText();
    void setupTextItem(QGraphicsTextItem *item, int left, int top, QColor color =Qt::white);
    QBrush* getBrushByColor(CellColor color);
    void askForNewGame();
    void pauseGame();
    void dropFigure();
    void moveFigureLeft();
    void moveFigureRight();
    void rotateFigureCW();
    void rotateFigureCCW();
};

#endif // MAINWINDOW_H
